package fr.L8GAN.Elteria;

import fr.trxyy.alternative.alternative_api.GameConnect;
import fr.trxyy.alternative.alternative_api.GameEngine;
import fr.trxyy.alternative.alternative_api.GameFolder;
import fr.trxyy.alternative.alternative_api.GameLinks;
import fr.trxyy.alternative.alternative_api.GameStyle;
import fr.trxyy.alternative.alternative_api.GameVersion;
import fr.trxyy.alternative.alternative_api.LauncherPreferences;
import fr.trxyy.alternative.alternative_api.maintenance.GameMaintenance;
import fr.trxyy.alternative.alternative_api.maintenance.Maintenance;
import fr.trxyy.alternative.alternative_api_ui.LauncherBackground;
import fr.trxyy.alternative.alternative_api_ui.LauncherPane;
import fr.trxyy.alternative.alternative_api_ui.base.AlternativeBase;
import fr.trxyy.alternative.alternative_api_ui.base.LauncherBase;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import net.arikia.dev.drpc.DiscordEventHandlers;
import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordRichPresence;
import net.arikia.dev.drpc.DiscordUser;
import net.arikia.dev.drpc.callbacks.ReadyCallback;

public class LauncherMain extends AlternativeBase {
	
	private GameFolder gameFolder = new GameFolder("elterialauncher");
	private LauncherPreferences launcherPreferences = new LauncherPreferences("Elteria Launcher", 950, 600, true);
	private GameEngine gameEngine = new GameEngine(gameFolder, launcherPreferences, GameVersion.V_1_12_2, GameStyle.FORGE_1_8_TO_1_12_2);
	private GameLinks gameLinks = new GameLinks("WEB LINK", "1.12.2.json");
	
	private GameMaintenance gameMaintenance = new GameMaintenance(Maintenance.USE, gameEngine);

	private GameConnect gameConnect = new GameConnect("SERVER IP", "25565");
	
	// Discord Tools
	private boolean running = true;
	

	public static void main(String[] args) {
		
		
		// DISCORD RP INIT 
		
        DiscordEventHandlers handlers = new DiscordEventHandlers.Builder().setReadyEventHandler((user) -> {
            System.out.println("Welcome " + user.username + "#" + user.discriminator + ".");
            DiscordRichPresence.Builder presence = new DiscordRichPresence.Builder("Entering credentials...");
            presence.setDetails("Starting Launcher");
            presence.setBigImage("logo", "Elteria");
            DiscordRPC.discordUpdatePresence(presence.build());
        }).build();
        DiscordRPC.discordInitialize("DISCORD ID", handlers, false);
        DiscordRPC.discordRegister("DISCORD ID", "");
        
        // DISCORD RP THREAD 
        
		new Thread(() -> {
			while (true)
			{
	            DiscordRPC.discordRunCallbacks();
			}
		}).start();
		
		launch(args);
	}

	
	
	@Override
	public void start(Stage primaryStage) throws Exception {
		
		
		Scene scene = new Scene(createContent());
		this.gameEngine.reg(primaryStage);
		
		this.gameEngine.reg(this.gameLinks);

		this.gameEngine.reg(this.gameMaintenance);
		
		this.gameEngine.reg(this.gameConnect);


		LauncherBase launcherBase = new LauncherBase(primaryStage, scene, StageStyle.UNDECORATED, gameEngine);
		launcherBase.setIconImage(primaryStage, getResourceLocation().loadImage(gameEngine, "favicon.png"));
		
		
	}
	
	private Parent createContent() {
		LauncherPane contentPane = new LauncherPane(gameEngine);
		new LauncherBackground(gameEngine, getResourceLocation().getMedia(gameEngine, "background.mp4"), contentPane);
		new LauncherPanel(contentPane, gameEngine);
		return contentPane;
	}
	
	


}