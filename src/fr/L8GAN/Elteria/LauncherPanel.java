package fr.L8GAN.Elteria;

import java.text.DecimalFormat;

import fr.trxyy.alternative.alternative_api.GameEngine;
import fr.trxyy.alternative.alternative_api.account.AccountType;
import fr.trxyy.alternative.alternative_api.auth.GameAuth;
import fr.trxyy.alternative.alternative_api.updater.GameUpdater;
import fr.trxyy.alternative.alternative_api.utils.FontLoader;
import fr.trxyy.alternative.alternative_api.utils.Mover;
import fr.trxyy.alternative.alternative_api_ui.LauncherAlert;
import fr.trxyy.alternative.alternative_api_ui.base.IScreen;
import fr.trxyy.alternative.alternative_api_ui.components.LauncherButton;
import fr.trxyy.alternative.alternative_api_ui.components.LauncherImage;
import fr.trxyy.alternative.alternative_api_ui.components.LauncherLabel;
import fr.trxyy.alternative.alternative_api_ui.components.LauncherPasswordField;
import fr.trxyy.alternative.alternative_api_ui.components.LauncherRectangle;
import fr.trxyy.alternative.alternative_api_ui.components.LauncherTextField;
import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.geometry.Pos;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.stage.Stage;
import net.arikia.dev.drpc.DiscordRPC;
import net.arikia.dev.drpc.DiscordRichPresence;

public class LauncherPanel extends IScreen {
	
	private LauncherRectangle topRectangle;
	
	private LauncherLabel titleLabel;
	private LauncherImage titleImage;
	
	private LauncherTextField usernameField;
	private LauncherPasswordField passwordField;
	
	private LauncherButton loginButton;
	private LauncherButton settingsButton;
	
	private LauncherButton closeButton;
	private LauncherButton reduceButton;
	
	private LauncherButton facebookButton;
	private LauncherButton twitterButton;
	private LauncherButton instagramBtton;
	private LauncherButton youtubeButton;
	
	private Timeline timeline;
	private DecimalFormat decimalFormat = new DecimalFormat(".#");
	private Thread updateThread;
	private GameUpdater gameUpdater = new GameUpdater();
	private LauncherRectangle updateRectangle;
	private LauncherLabel updateLabel;
	private LauncherLabel currentFileLabel;
	private LauncherLabel percentageLabel;
	private LauncherLabel currentStep;
	

	
	public LauncherPanel(Pane root, GameEngine engine) {
	
		// Discord ADDON 
		
		this.topRectangle = new LauncherRectangle(root, 0, 0, engine.getWidth(), 31);
		this.topRectangle.setFill(Color.rgb(0, 0, 0, 0.65));
		
		this.drawLogo(engine, getResourceLocation().loadImage(engine, "alternative_logo.png"), engine.getWidth() / 2 - 150, 30, 330, 235, root, Mover.MOVE);
		
		this.titleLabel = new LauncherLabel(root);
		this.titleLabel.setText("ELTERIA Launcher");
		this.titleLabel.setFont(FontLoader.loadFont("Confortaa-Regular.ttf", "Confortaa", 18f));
		this.titleLabel.setStyle("-fx-background-color: transparent; -fx-text-fill: white");
		this.titleLabel.setPosition(engine.getWidth() / 2 - 80, -4);
		this.titleLabel.setOpacity(0.7);
		this.titleLabel.setSize(500, 40);
		
		this.titleImage = new LauncherImage(root);
		this.titleImage.setImage(getResourceLocation().loadImage(engine, "favicon.png"));
		this.titleImage.setSize(25, 25);
		this.titleImage.setPosition(engine.getWidth() / 3 + 40, 3);
		
		this.usernameField = new LauncherTextField(root);
		this.usernameField.setPosition(engine.getWidth() / 2 - 135, engine.getHeight() / 2 - 57);
		this.usernameField.setSize(270, 50);
		this.usernameField.setFont(FontLoader.loadFont("Confortaa-Regular.ttf", "Confortaa", 14f));
		this.usernameField.setStyle("-fx-background-color: rgba(3, 0, 0, 0.4); -fx-text-fill: white;");
		this.usernameField.setVoidText("Nom de Compte");
		
		this.passwordField = new LauncherPasswordField(root);
		this.passwordField.setPosition(engine.getWidth() / 2 - 135, engine.getHeight() / 2);
		this.passwordField.setSize(270, 50);
		this.passwordField.setFont(FontLoader.loadFont("Confortaa-Regular.ttf", "Confortaa", 14f));
		this.passwordField.setStyle("-fx-background-color: rgba(0, 0, 0, 0.4); -fx-text-fill: white;");
		this.passwordField.setVoidText("Mot de Passe (Aucun = Version Crack)");
		
		this.loginButton = new LauncherButton(root);
		this.loginButton.setText("Se Connecter");
		this.loginButton.setFont(FontLoader.loadFont("Confortaa-Regular.ttf", "Confortaa", 22f));
		this.loginButton.setPosition(engine.getWidth() / 2 - 67, engine.getHeight() / 2 + 60);
		this.loginButton.setSize(200, 35);
		this.loginButton.setStyle("-fx-background-color: rgba(0, 0, 0, 0.4); -fx-text-fill: white;");
		this.loginButton.setAction(event -> {
			if (this.usernameField.getText().length() < 3) {
				new LauncherAlert("Echec de Connexion","La case du Pseudo doit contenir plus de 3 caractères.");
			}
			else if (this.usernameField.getText().length() > 3 && this.passwordField.getText().isEmpty()) {
				GameAuth auth = new GameAuth(this.usernameField.getText(), this.passwordField.getText(), AccountType.OFFLINE);
				if (auth.isLogged()) {
					this.update(engine, auth);
				}
			}
			else if (this.usernameField.getText().length() > 3 && !this.passwordField.getText().isEmpty()) {
				GameAuth auth = new GameAuth(this.usernameField.getText(), this.passwordField.getText(), AccountType.MOJANG);
				if (auth.isLogged()) {
					this.update(engine, auth);
				}
				else {
					new LauncherAlert("Echec de la Connexion", "Identifiants Incorrects");
				}
			}
			else {
				new LauncherAlert("Echec de la Connexion", "La Connexion a échouée.");
			}
		});
		
		this.settingsButton = new LauncherButton(root);
		this.settingsButton.setStyle("-fx-background-color: rgba(0, 0, 0, 0.4); -fx-text-fill: white;");
		LauncherImage settingsImg = new LauncherImage(root, getResourceLocation().loadImage(engine, "settings.png"));
		settingsImg.setSize(27, 27);
		this.settingsButton.setGraphic(settingsImg);
		this.settingsButton.setPosition(engine.getWidth() / 2 - 135, engine.getHeight() / 2 + 60);
		this.settingsButton.setSize(60, 48);
		
		this.closeButton = new LauncherButton(root);
		this.closeButton.setInvisible();
		this.closeButton.setPosition(engine.getWidth() - 35, 2);
		this.closeButton.setSize(15, 15);
		this.closeButton.setBackground(null);
		LauncherImage closeIMG = new LauncherImage(root, getResourceLocation().loadImage(engine, "close.png"));
		closeIMG.setSize(15, 15);
		this.closeButton.setGraphic(closeIMG);
		this.closeButton.setOnAction(event -> {
			System.exit(0);
		});
		
		this.reduceButton = new LauncherButton(root);
		this.reduceButton.setInvisible();
		this.reduceButton.setPosition(engine.getWidth() - 58, 2);
		this.reduceButton.setSize(15, 15);
		this.reduceButton.setBackground(null);
		LauncherImage reduceImg = new LauncherImage(root, getResourceLocation().loadImage(engine, "reduce.png"));
		reduceImg.setSize(15, 15);
		this.reduceButton.setGraphic(reduceImg);
		this.reduceButton.setOnAction(event -> {
			Stage stage = (Stage) ((LauncherButton) event.getSource()).getScene().getWindow();
			stage.setIconified(true);
		});
		
		this.facebookButton = new LauncherButton(root);
		this.facebookButton.setInvisible();
		this.facebookButton.setPosition(engine.getWidth() / 2 - 125, engine.getHeight() - 130);
		LauncherImage fbImg = new LauncherImage(root, getResourceLocation().loadImage(engine, "fb_icon.png"));
		fbImg.setSize(80, 80);
		this.facebookButton.setGraphic(fbImg);
		this.facebookButton.setSize((int)fbImg.getFitWidth(), (int)fbImg.getFitHeight());
		this.facebookButton.setBackground(null);
		this.facebookButton.setOnAction(event -> {
			openLink("https://www.facebook.com/elteria.serveur");
		});
		
		this.twitterButton = new LauncherButton(root);
		this.twitterButton.setInvisible();
		this.twitterButton.setPosition(engine.getWidth() / 2 + 25, engine.getHeight() - 130);
		LauncherImage twitterImg = new LauncherImage(root, getResourceLocation().loadImage(engine, "twitter_icon.png"));
		twitterImg.setSize(95, 80);
		this.twitterButton.setGraphic(twitterImg);
		this.twitterButton.setSize((int)twitterImg.getFitWidth(), (int)twitterImg.getFitHeight());
		this.twitterButton.setBackground(null);
		this.twitterButton.setOnAction(event -> {
			openLink("https://twitter.com/ElteriaS");
		});
		
		this.instagramBtton = new LauncherButton(root);
		this.instagramBtton.setInvisible();
		this.instagramBtton.setPosition(engine.getWidth() / 2 + 187, engine.getHeight() - 130);
		LauncherImage instaImg = new LauncherImage(root, getResourceLocation().loadImage(engine, "insta_icon.png"));
		instaImg.setSize(80, 80);
		this.instagramBtton.setGraphic(instaImg);
		this.instagramBtton.setSize((int)instaImg.getFitWidth(), (int)instaImg.getFitHeight());
		this.instagramBtton.setBackground(null);
		this.instagramBtton.setOnAction(event -> {
			openLink("https://www.instagram.com/elteriaserveur/");
		});
		
		this.youtubeButton = new LauncherButton(root);
		this.youtubeButton.setInvisible();
		this.youtubeButton.setPosition(engine.getWidth() / 2 - 150 - 150, engine.getHeight() - 130);
		LauncherImage ytImg = new LauncherImage(root, getResourceLocation().loadImage(engine, "yt_icon.png"));
		ytImg.setSize(110, 80);
		this.youtubeButton.setGraphic(ytImg);
		this.youtubeButton.setSize((int)ytImg.getFitWidth(), (int)instaImg.getFitHeight());
		this.youtubeButton.setBackground(null);
		this.youtubeButton.setOnAction(event -> {
			openLink("https://www.youtube.com/channel/UCDYGAI6p0gO_-5bACLUF8Rw");
		});
		
		this.updateRectangle = new LauncherRectangle(root, engine.getWidth() / 2 - 175, engine.getHeight() / 2 - 60, 350, 210);
		this.updateRectangle.setArcWidth(10.0);
		this.updateRectangle.setArcHeight(10.0);
		this.updateRectangle.setFill(Color.rgb(0, 0, 0, 0.60));
		this.updateRectangle.setVisible(false);
		
		this.updateLabel = new LauncherLabel(root);
		this.updateLabel.setText("- MISE A JOUR -");
		this.updateLabel.setAlignment(Pos.CENTER);
		this.updateLabel.setFont(FontLoader.loadFont("Confortaa-Regular.ttf", "Confortaa", 22F));
		this.updateLabel.setStyle("-fx-background-color: transparent; -fx-text-fill: white;");
		this.updateLabel.setPosition(engine.getWidth() / 2 - 95, engine.getHeight() / 2 - 55);
		this.updateLabel.setSize(190, 40);
		this.updateLabel.setVisible(false);
		
		this.currentStep = new LauncherLabel(root);
		this.currentStep.setText("Pr�paration de la Mise � jour...");
		this.currentStep.setFont(Font.font("Verdana", FontPosture.ITALIC, 18F));
		this.updateLabel.setStyle("-fx-background-color: transparent; -fx-text-fill: white;");
		this.currentStep.setAlignment(Pos.CENTER);
		this.currentStep.setPosition(engine.getWidth() / 2 - 160, engine.getHeight() / 2 + 83);
		this.currentStep.setOpacity(0.4);
		this.currentStep.setSize(320, 40);
		this.currentStep.setVisible(false);
		
		this.currentFileLabel = new LauncherLabel(root);
		this.currentFileLabel.setText("");
		this.currentFileLabel.setAlignment(Pos.CENTER);
		this.currentFileLabel.setFont(FontLoader.loadFont("Confortaa-Regular.ttf", "Confortaa", 18F));
		this.currentFileLabel.setStyle("-fx-background-color: transparent; -fx-text-fill: white;");
		this.currentFileLabel.setPosition(engine.getWidth() / 2 - 160, engine.getHeight() / 2 + 25);
		this.currentFileLabel.setSize(320, 40);
		this.currentFileLabel.setVisible(false);
		
		this.percentageLabel = new LauncherLabel(root);
		this.percentageLabel.setText("0%");
		this.percentageLabel.setAlignment(Pos.CENTER);
		this.percentageLabel.setFont(FontLoader.loadFont("Confortaa-Regular.ttf", "Confortaa", 30F));
		this.percentageLabel.setStyle("-fx-background-color: transparent; -fx-text-fill: white;");
		this.percentageLabel.setPosition(engine.getWidth() / 2 - 50, engine.getHeight() / 2 - 5);
		this.percentageLabel.setOpacity(0.8);
		this.percentageLabel.setSize(100, 40);
		this.percentageLabel.setVisible(false);
		
	}
	
	public void update(GameEngine engine, GameAuth auth) {
		this.usernameField.setDisable(true);
		this.usernameField.setVisible(false);
		this.passwordField.setDisable(true);
		this.passwordField.setVisible(false);
		this.loginButton.setDisable(true);
		this.loginButton.setVisible(false);
		this.settingsButton.setDisable(true);
		this.settingsButton.setVisible(false);
		
		this.updateRectangle.setVisible(true);
		this.updateLabel.setVisible(true);
		this.currentStep.setVisible(true);
		this.currentFileLabel.setVisible(true);
		this.percentageLabel.setVisible(true);
		
		gameUpdater.reg(engine);
		gameUpdater.reg(auth.getSession());
		engine.reg(this.gameUpdater);
		this.updateThread = new Thread() {
			public void run() {
				engine.getGameUpdater().run();
			}
		};
		this.updateThread.start();
		
		this.timeline = new Timeline(new KeyFrame[] {
		new KeyFrame(javafx.util.Duration.seconds(0.0D), e -> updateDownload(engine),
		new javafx.animation.KeyValue[0]),
		new KeyFrame (javafx.util.Duration.seconds(0.1D),
		new javafx.animation.KeyValue[0])});
		this.timeline.setCycleCount(Animation.INDEFINITE);
		this.timeline.play();
	}

	private void updateDownload(GameEngine engine) {
		if (engine.getGameUpdater().downloadedFiles > 0) {
			this.percentageLabel.setText(decimalFormat.format(engine.getGameUpdater().downloadedFiles * 100.0D / engine.getGameUpdater().filesToDownload) + "%");
		} else {
			this.percentageLabel.setText("99%");
		}
		this.currentFileLabel.setText(engine.getGameUpdater().getCurrentFile());
		this.currentStep.setText(engine.getGameUpdater().getCurrentInfo());
		
		//TODO : Change game Image later... 
		//		 Edit game IP
		
        DiscordRichPresence.Builder presence = new DiscordRichPresence.Builder("Playing mc.elteria.fr");
        presence.setDetails("In Game");
        presence.setBigImage("logo", "Change game Image...");
        DiscordRPC.discordUpdatePresence(presence.build());
	}
	
}